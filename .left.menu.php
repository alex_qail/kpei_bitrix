<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$aMenuLinks = Array(
    Array(
        "Новости",
        "news/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Компания",
        "#",
        Array(),
        Array("CHILDREN" => Array(
            Array(
                "TEXT" => "О компании",
                "LINK" => "company/",
                Array(),
                Array(),
                ""
            ),
            Array(
                "TEXT" => "Руководство",
                "LINK" => "management/",
                Array(),
                Array(),
                ""
            ),
            Array(
                "TEXT" => "Ресурсы",
                "LINK" => "resources/",
                Array(),
                Array(),
                ""
            )
        )),
        ""
    ),
    Array(
        "Проекты",
        "projects/",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Карьера",
        "career/",
        Array(),
        Array(),
        ""
    ),
	Array(
		"Контакты", 
		"contacts/", 
		Array(), 
		Array(), 
		"" 
	),
    Array(
        "Поиск по сайту",
        "#",
        Array(),
        Array("TYPE" => "SEARCH", "CLASS" => "search__toggle"),
        "",
        "CLASS" => "search__toggle"
    ),
    Array(
        "+7 (383) 349-38-53",
        "#",
        Array(),
        Array("TYPE" => "phone", "CLASS" => "phone__link", "ADDITIONAL_TEXT" => "Новосибирск"),
        "",
        "CLASS" => "phone__link"
    )
);
?>