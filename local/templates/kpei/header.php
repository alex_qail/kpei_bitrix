<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?$APPLICATION->ShowHead();?>
<link href="<?=SITE_TEMPLATE_PATH?>/css/selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?=SITE_TEMPLATE_PATH?>/css/app.css" type="text/css" rel="stylesheet" />
<link href="<?=SITE_TEMPLATE_PATH?>/css/slider.css" type="text/css" rel="stylesheet" />
<?php
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/plugins.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/forms.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/animation.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/common.min.js");
?>
	<!--[if lte IE 6]>
	<style type="text/css">
		
		#banner-overlay { 
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/overlay.png', sizingMethod = 'crop'); 
		}
		
		div.product-overlay {
			background-image: none;
			filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?=SITE_TEMPLATE_PATH?>images/product-overlay.png', sizingMethod = 'crop');
		}
		
	</style>
	<![endif]-->

	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="app">
		<header class="header">

            <div class="logo__box">
                <a href="<?=SITE_DIR?>" title="<?=GetMessage('CFT_MAIN')?>"><?
                    $APPLICATION->IncludeFile(
                        SITE_TEMPLATE_PATH."/include/company_name.php",
                        Array(),
                        Array("MODE"=>"html")
                    );
                    ?>
                </a>
            </div>

            <?$APPLICATION->IncludeComponent("bitrix:menu", "left", array(
                "ROOT_MENU_TYPE" => "left",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "0",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "Y",
                "ALLOW_MULTI_SELECT" => "N"
            ),
                false,
                array(
                    "ACTIVE_COMPONENT" => "Y"
                )
            );?>

		</header>
