<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>

                <footer class="footer">
    <img src="images/footer-pattern-left.svg" width="203" height="452" alt="" loading="lazy" class="horizon"
         data-animate-in="transY:50;delay: 600">
    <img src="images/footer-pattern-right.svg" width="763" height="1573" alt="" loading="lazy" class="horizon"
         data-animate-in="transY:50;delay: 800">
    <div class="container relative z-index-1">
        <div class="row mb-40 mb-sm-60">
            <div class="col-sm-12 col-md-8 col-lg-6 pt-sm-30 horizon" data-animate-in="transY:50;">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom1",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "0",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-6 pt-sm-30 horizon" data-animate-in="transY:50;delay: 200">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
                    "ROOT_MENU_TYPE" => "bottom2",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "0",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "1",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                    false,
                    array(
                        "ACTIVE_COMPONENT" => "Y"
                    )
                );?>
            </div>
            <div class="col-md-8 col-lg-7 horizon pt-20 pt-sm-0" data-animate-in="transY:50;delay: 400">
                <div class="text-muted mb-5">Адрес</div>
                <div class="h5 font-weight-medium mb-sm-40">
                    630087, Новосибирск, <br>
                    ул. Немировича-Данченко, 165, <br>
                    оф.&nbsp;715
                </div>
                <div class="text-muted mb-5">Адрес эл. почты</div>
                <div class="h5 font-weight-medium mb-sm-40">
                    info@k-pei.ru
                </div>
                <div class="text-muted font-weight-medium">&copy;&nbsp;2021, Компания ПроектЭнергоИнжиниринг</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm col-md-16 col-lg-12 font-weight-medium mb-20 mb-md-0">
                <a href="">Политика конфеденциальности</a>
            </div>
            <div class="col-sm-auto col-md-8 col-lg-6">
                Создание сайта —
                <a href="" class="d-inline-flex align-items-center font-weight-medium">
                    Евростудио
                    <span class="ml-10">
                  <svg width="3rem" height="3rem" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M23.213 2.464A15.011 15.011 0 006.83 2.402L9.36 4.943a11.451 11.451 0 0111.284.085l2.57-2.564zm4.364 20.74a15.01 15.01 0 000-16.356l-2.602 2.598a11.456 11.456 0 01-.045 11.106l2.647 2.653zM0 14.97a15.022 15.022 0 0023.213 12.602l-2.649-2.654A11.457 11.457 0 014.98 9.309L2.438 6.768A14.906 14.906 0 000 14.969zm10.653.023a4.376 4.376 0 114.377 4.384 4.363 4.363 0 01-4.377-4.385zm-3.506-.008a7.875 7.875 0 107.884-7.856 7.894 7.894 0 00-7.883 7.856h-.001z"
                          fill="#FF9600" />
                  </svg>
                </span>
                </a>
            </div>
        </div>
    </div>
</footer>

            </div>
        </div>
    </body>
</html>