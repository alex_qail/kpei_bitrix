<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (empty($arResult)) return;

$arMenu = array();
$first = true;
foreach($arResult as $itemIndex => $arItem)
{
	if ($arItem["PERMISSION"] > "D" && $arItem["DEPTH_LEVEL"] == 1)
	{
		$className = '';
		if ($first) {$className .= ' first-item'; $first = false;}
		if ($arItem['SELECTED']) {$className .= ' selected';}
		
		$arItem['CLASS'] = $className;
		$arMenu[] = $arItem;
	}
}

if (empty($arMenu)) return;

$arMenu[count($arMenu)-1]['CLASS'] .= ' last-item';
?>
<nav>
    <ul class="nav">
        <?
            foreach($arMenu as $arItem):
        ?>
            <?php
                $linkClass = $arItem['PARAMS']['CLASS'] ? $arItem['PARAMS']['CLASS'] : "nav__item";
                $itemClass = !empty($arItem["PARAMS"]["CHILDREN"]) ? "nav__dropdown" : "nav__item-wrapper";
                if (!empty($arItem['PARAMS']['TYPE']))
                {
                    $itemClass.= " " . $arItem['PARAMS']['TYPE'];
                }
            ?>
            <li class="<?= $itemClass ?>">
                <a class="<?= $linkClass ?>" href="<?=$arItem["LINK"]?>">
                    <?=$arItem["TEXT"]?>
                </a>
                <?php if(!empty($arItem["PARAMS"]["ADDITIONAL_TEXT"])): ?>
                    <?= $arItem["PARAMS"]["ADDITIONAL_TEXT"] ?>
                <?php endif; ?>
                <?php if(!empty($arItem["PARAMS"]["CHILDREN"])): ?>
                    <div class="submenu">
                        <ul class="submenu__list">
                            <?php foreach($arItem["PARAMS"]["CHILDREN"] as $childItem):?>
                                <li>
                                    <a href="<?=$childItem["LINK"]?>">
                                        <?=$childItem["TEXT"]?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </li>
        <?php endforeach ?>
    </ul>
</nav>