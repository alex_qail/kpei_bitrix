<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="features">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $blockKey => $arItem):?>
	<?
    $isEvenBlock = ($blockKey+1)%2 == 0;
    $additionalRowClass = $isEvenBlock ? " flex-row-reverse justify-content-end" : "";
    $textColClass = $isEvenBlock ? "col-24 col-md-10 col-lg-9 mr-md-40 mr-lg-180 text-md tmh-perspective" : "col-24 col-md-10 col-lg-9 ml-md-40 ml-lg-60 text-md tmh-perspective";
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <section class="container line line_about relative" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="row align-items-center relative<?= $additionalRowClass ?>">
            <div class="col-22 col-sm-16 col-md-12 col-lg-9">
                <div class="features__img <?= FRONT_PAGE_BLOCKS_CLASSES[$blockKey] ?>">
                    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                        <img
                            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                            width="540"
                            height="540"
                            loading="lazy"
                            class="horizon"
                            border="0"
                            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                            />
                    <?= file_get_contents($_SERVER["DOCUMENT_ROOT"] . CFile::GetPath($arItem["PROPERTIES"]["BLOCK_SVG_GRAPHICS"]["VALUE"])) ?>
                    <?endif?>
                </div>
            </div>
            <div class="<?= $textColClass ?>">
                <h3 class="features__title horizon" data-animate-in="transY:50;delay: 200">
                    <?= $arItem["NAME"] ?>
                </h3>
                <?= $arItem["PREVIEW_TEXT"] ?>
            </div>
        </div>
    </section>
<?endforeach;?>
</section>
