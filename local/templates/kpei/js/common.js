let initialScroll = 0;
const $b = document.body;
const $a = document.querySelector('.app');

function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

ready(() => {
  /* Функция открытия и закрытия меню */
  function menuToggle(e) {
    e.preventDefault();
    initialScroll = window.scrollY;
    $b.classList.remove('search-on', 'catalog-on');
    $b.classList.toggle('menu-on');
    if (window.matchMedia('(max-width: 1024px)').matches) {
      if ($b.classList.contains('menu-on')) {
        window.scrollTo({
          top: 0,
        });
        $a.style.top = '-' + initialScroll + 'px';
      } else {
        let backScroll = $a.style.top.substring(1, $a.style.top.length - 2);
        $a.style.top = '';
        window.scrollTo({
          top: backScroll,
        });
      }
    }
  }

  if (document.querySelectorAll('.menu__toggle').length) {
    document.querySelectorAll('.menu__toggle').forEach((btn) => {
      btn.addEventListener('click', (e) => {
        menuToggle(e);
      });
    });
  }

  /* Функция переключения табов */
  if (document.querySelectorAll('.tabs__item').length) {
    if (document.querySelectorAll('.tabs__content')) {
      document.querySelector('.tabs__content').classList.add('tabs__content_active');
      document.querySelector('.tabs__item').classList.add('tabs__item_active');
    }

    document.querySelectorAll('.tabs__item').forEach((tab) => {
      tab.addEventListener('click', (e) => {
        e.preventDefault();
        let contentBox = tab.getAttribute('data-tab');
        document.querySelectorAll('.tabs__item').forEach((item) => {
          item.classList.remove('tabs__item_active');
        });
        document.querySelectorAll('.tabs__content').forEach((content) => {
          content.classList.remove('tabs__content_active');
        });
        document.querySelector(contentBox).classList.add('tabs__content_active');
        tab.classList.add('tabs__item_active');
        if (tab.classList.contains('tabs__item_active')) {
          document.querySelector('.tabs').classList.toggle('active');
        }
        else {
          document.querySelector('.tabs').classList.remove('active');
        }
      });
    });

  }

  /* Функция определения переполнения раскрывающихся блоков */
  function checkOverflowBox(element) {
    let el = document.querySelectorAll(element);
    el.forEach((box) => {
      let height = box.scrollHeight;
      box.style.setProperty('--mh-auto', height + 'px');
      if (!box.classList.contains('opened')) {
        box.clientHeight >= box.scrollHeight
          ? box.classList.add('not-overflowed')
          : box.classList.remove('not-overflowed');
      }
    });
  }

  function setOverflowBox(element) {
    if (document.querySelectorAll(element).length) {
      checkOverflowBox(element);
      window.addEventListener('resize', () => {
        checkOverflowBox(element);
      });
    }
  }

  setOverflowBox('.expand__box');

  /* Функция открытия/закрытия раскрывающихся блоков */
  document.querySelectorAll('.expand__toggle').forEach((toggle) => {
    toggle.addEventListener('click', (e) => {
      e.preventDefault();
      let boxExpand = toggle.getAttribute('data-expand');
      let box = document.querySelector(boxExpand);
      box.classList.toggle('opened');
      toggle.classList.toggle('opened');
      if (
        !box.classList.contains('opened') &&
        box.scrollHeight > window.innerHeight
      ) {
        let headerOffset = document.querySelector('header').offsetHeight;
        let elementPosition = box.parentElement.getBoundingClientRect().top;
        let offsetPosition =
          window.scrollY + elementPosition - headerOffset * 1.2;
        window.scrollTo({
          top: offsetPosition,
          behavior: 'smooth',
        });
      }
    });
  });

  // Кнопка Наверх
  window.addEventListener('scroll', () => {
    document
      .querySelectorAll('body')[0]
      .classList[window.pageYOffset > 500 ? 'add' : 'remove']('ontop-on');
  });

  document.querySelectorAll('.on-top').forEach((modal) => {
    modal.addEventListener('click', (e) => {
      e.preventDefault();
      scrollToElement(0, 1000);
    });
  });

  //появление объектов
  if ($('.horizon').get(0)) {
    $('.horizon').horizon({
        threshold: .5,
        easing: 'easeInOutCubic',
        recurring: false,
        duration: 500
    });
  }

  /* Карусель партнеров */
  if (document.querySelectorAll('.carousel__partners').length) {
    var slider = tns({
      container: '.carousel__partners',
      items: 4,
      prevButton: '.partners_prev',
      nextButton: '.partners_next',
      nav: false,
      gutter: 20,
      responsive: {
        900: {
          items: 5,
        },
      },
    });
  }

});
