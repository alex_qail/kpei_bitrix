<?php
define("FRONT_PAGE_BLOCKS_IBLOCK_ID", 8);
define("COMPANY_IN_NUMBERS_IBLOCK_ID", 7);
define("PROJECT_IN_NUMBERS_IBLOCK_ID", 6);
define("PROJECT_IN_NUMBERS_DELAY_STEP", 200);

define("FRONT_PAGE_BLOCKS_CLASSES", array(
    "features__img_first",
    "features__img_second",
    "features__img_third",
    "features__img_fourth",
    "features__img_fifth"
));