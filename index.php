<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
    <div class="page__wrapper">
        <div class="page">

            <section class="main-animation loading">
                <div class="main-animation__video tab-1">
                    <video poster="<?= SITE_TEMPLATE_PATH ?>/storage/01.png" muted>
                        <source src="<?= SITE_TEMPLATE_PATH ?>/video/01.mp4" type='video/mp4'>
                    </video>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/storage/01.png" alt="">
                </div>
                <div class="main-animation__video tab-2">
                    <video poster="<?= SITE_TEMPLATE_PATH ?>/storage/02.png" muted>
                        <source src="<?= SITE_TEMPLATE_PATH ?>/video/02.mp4" type='video/mp4'>
                    </video>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/storage/02.png" alt="">
                </div>
                <div class="main-animation__video tab-3">
                    <video poster="<?= SITE_TEMPLATE_PATH ?>/storage/03.png" muted>
                        <source src="<?= SITE_TEMPLATE_PATH ?>/video/03.mp4" type='video/mp4'>
                    </video>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/storage/03.png" alt="">
                </div>
                <div class="main-animation__video tab-4">
                    <video poster="<?= SITE_TEMPLATE_PATH ?>/storage/04.png" muted>
                        <source src="<?= SITE_TEMPLATE_PATH ?>/video/04.mp4" type='video/mp4'>
                    </video>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/storage/04.png" alt="">
                </div>
                <div class="main-animation__pattern">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/main-banner-pattern.svg" width="1451" height="813" alt="" loading="lazy">
                </div>
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/main-pattern.svg" width="1309" height="3208" alt="" loading="lazy">
                <div class="main-animation__tabs">
                    <div class="main-animation__tab tab-1">
                        <div class="main-animation__link" data-slide=".tab-1">
                            Объекты электрогенерации
                        </div>
                        <div class="main-animation__box">
                            <div class="main-animation__title">
                                Объекты электрогенерации
                            </div>
                            <div class="main-animation__text">
                                <p>Проектирование электростанций</p>
                                <a href="" class="arrow__link">
                                    <svg width="3.7rem" height="3rem" viewBox="0 0 37 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M17.564 12.408L6.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156L9.844 15l-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0018.628 15c.003-.94-.351-1.88-1.064-2.592z"
                                              fill="#FF9600" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M35.564 12.408L24.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156l8.78 8.78-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0036.628 15a3.635 3.635 0 00-1.064-2.592z"
                                              fill="#FF9600" />
                                    </svg>
                                    Смотреть проекты
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="main-animation__tab tab-2">
                        <div class="main-animation__link" data-slide=".tab-2">
                            Подстанционные объекты
                        </div>
                        <div class="main-animation__box">
                            <div class="main-animation__title">
                                Подстанционные объекты
                            </div>
                            <div class="main-animation__text">
                                <p>Проектирование строительства/реконструкции подстанций 35-750&nbsp;кВ
                                </p>
                                <p>Проектирование автоматизированных систем учета и&nbsp;управления (АИИС КУЭ, АСТУЭ, АСДУ,
                                    АСУ&nbsp;ТП)</p>
                                <a href="" class="arrow__link">
                                    <svg width="3.7rem" height="3rem" viewBox="0 0 37 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M17.564 12.408L6.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156L9.844 15l-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0018.628 15c.003-.94-.351-1.88-1.064-2.592z"
                                              fill="#FF9600" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M35.564 12.408L24.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156l8.78 8.78-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0036.628 15a3.635 3.635 0 00-1.064-2.592z"
                                              fill="#FF9600" />
                                    </svg>
                                    Смотреть проекты
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="main-animation__tab tab-3">
                        <div class="main-animation__link" data-slide=".tab-3">
                            Линии электропередач
                        </div>
                        <div class="main-animation__box">
                            <div class="main-animation__title">
                                Линии электропередач
                            </div>
                            <div class="main-animation__text">
                                <p>Проектирование строительства/реконструкции кабельных и&nbsp;воздушных линий электропередач 35-500&nbsp;кВ</p>
                                <a href="" class="arrow__link">
                                    <svg width="3.7rem" height="3rem" viewBox="0 0 37 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M17.564 12.408L6.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156L9.844 15l-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0018.628 15c.003-.94-.351-1.88-1.064-2.592z"
                                              fill="#FF9600" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M35.564 12.408L24.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156l8.78 8.78-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0036.628 15a3.635 3.635 0 00-1.064-2.592z"
                                              fill="#FF9600" />
                                    </svg>
                                    Смотреть проекты
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="main-animation__tab tab-4">
                        <div class="main-animation__link" data-slide=".tab-4">
                            Инженерные сети
                        </div>
                        <div class="main-animation__box">
                            <div class="main-animation__title">
                                Инженерные сети
                            </div>
                            <div class="main-animation__text">
                                <p>Проектирование строительства/реконструкции водопроводных и&nbsp;канализационных сетей и&nbsp;сооружений. Проектирование строительства/реконструкции тепловых сетей. Проектирование систем связи в&nbsp;энергетике. Проектирование охранно-пожарных систем</p>
                                <a href="" class="arrow__link">
                                    <svg width="3.7rem" height="3rem" viewBox="0 0 37 30" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M17.564 12.408L6.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156L9.844 15l-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0018.628 15c.003-.94-.351-1.88-1.064-2.592z"
                                              fill="#FF9600" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M35.564 12.408L24.22 1.064a3.657 3.657 0 00-5.156 0 3.657 3.657 0 000 5.156l8.78 8.78-8.78 8.78a3.657 3.657 0 000 5.156 3.657 3.657 0 005.156 0l11.344-11.344A3.635 3.635 0 0036.628 15a3.635 3.635 0 00-1.064-2.592z"
                                              fill="#FF9600" />
                                    </svg>
                                    Смотреть проекты
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?$APPLICATION->IncludeComponent("bitrix:news.list",
                    "company_in_numbers",
                    array(
                        "IBLOCK_TYPE" => "news",
                        "IBLOCK_ID" => COMPANY_IN_NUMBERS_IBLOCK_ID,
                        "USE_SEARCH" => "N",
                        "USE_RSS" => "N",
                        "NEWS_COUNT" => "5",
                        "NUM_NEWS" => "20",
                        "NUM_DAYS" => "30",
                        "YANDEX" => "N",
                        "USE_RATING" => "N",
                        "USE_CATEGORIES" => "N",
                        "USE_FILTER" => "N",
                        "SORT_BY1" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER2" => "ASC",
                        "CHECK_DATES" => "Y",
                        "SEF_MODE" => "Y",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_SHADOW" => "N",
                        "AJAX_OPTION_JUMP" => "Y",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "Y",
                        "CACHE_TYPE" => "N",
                        "CACHE_TIME" => "0",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "DISPLAY_PANEL" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "Y",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "USE_PERMISSIONS" => "N",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "LIST_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "PROPERTY_CODE" => array(
                            0 => "BLOCK_SVG_GRAPHICS",
                            1 => "",
                        ),
                        "LIST_PROPERTY_CODE" => array(
                            0 => "BLOCK_SVG_GRAPHICS",
                            1 => "",
                        ),
                        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                        "DISPLAY_NAME" => "Y",
                        "META_KEYWORDS" => "-",
                        "META_DESCRIPTION" => "-",
                        "BROWSER_TITLE" => "-",
                        "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "DETAIL_FIELD_CODE" => array(
                            0 => "",
                            1 => "",
                        ),
                        "DETAIL_PROPERTY_CODE" => array(
                            0 => "BLOCK_SVG_GRAPHICS",
                            1 => "",
                        ),
                        "DETAIL_DISPLAY_TOP_PAGER" => "N",
                        "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                        "DETAIL_PAGER_TITLE" => "Страница",
                        "DETAIL_PAGER_TEMPLATE" => "arrows",
                        "DETAIL_PAGER_SHOW_ALL" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "SEF_URL_TEMPLATES" => array(
                            "news" => "",
                            "section" => "",
                            "detail" => "#ELEMENT_ID#/",
                            "search" => "search/",
                            "rss" => "rss/",
                            "rss_section" => "#SECTION_ID#/rss/",
                        )
                    ),
                    false
                );?>

            <section class="features">
                <?$APPLICATION->IncludeComponent("bitrix:news.list", "front_page_blocks", array(
                    "IBLOCK_TYPE" => "news",
                    "IBLOCK_ID" => FRONT_PAGE_BLOCKS_IBLOCK_ID,
                    "USE_SEARCH" => "N",
                    "USE_RSS" => "N",
                    "NEWS_COUNT" => "5",
                    "NUM_NEWS" => "20",
                    "NUM_DAYS" => "30",
                    "YANDEX" => "N",
                    "USE_RATING" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_FILTER" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "CHECK_DATES" => "Y",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => "/news/",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_SHADOW" => "N",
                    "AJAX_OPTION_JUMP" => "Y",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "Y",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "0",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_PANEL" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "USE_PERMISSIONS" => "N",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "PROPERTY_CODE" => array(
                        0 => "BLOCK_SVG_GRAPHICS",
                        1 => "",
                    ),
                    "LIST_PROPERTY_CODE" => array(
                        0 => "BLOCK_SVG_GRAPHICS",
                        1 => "",
                    ),
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "-",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "DETAIL_PROPERTY_CODE" => array(
                        0 => "BLOCK_SVG_GRAPHICS",
                        1 => "",
                    ),
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PAGER_TEMPLATE" => "arrows",
                    "DETAIL_PAGER_SHOW_ALL" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Новости",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "SEF_URL_TEMPLATES" => array(
                        "news" => "",
                        "section" => "",
                        "detail" => "#ELEMENT_ID#/",
                        "search" => "search/",
                        "rss" => "rss/",
                        "rss_section" => "#SECTION_ID#/rss/",
                    )
                ),
                    false
                );?>
            </section>
        </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>